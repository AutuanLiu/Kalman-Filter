x1(t) = 1.3579 * x1(t-1) + -0.9124 * x1(t-2) + -0.0096 * x1(t-4) + -0.0009 * x2(t-3)*x2(t-4) + -0.0015 * x3(t-5)*x4(t-1) + e1(t)
x2(t) = 0.3824 * x2(t-1) + 0.2271 * x2(t-4) + -0.4252 * x4(t-4) + 0.2996 * x4(t-5) + 0.0252 * x2(t-4)*x5(t-5) + e2(t)
x3(t) = -0.3793 * x1(t-3) + -0.0273 * x3(t-3) + 0.0031 * x1(t-1)*x2(t-2) + -0.0018 * x2(t-3)*x5(t-5) + -0.0007 * x4(t-1)*x5(t-5) + e3(t)
x4(t) = 0.2854 * x4(t-1) + 0.2885 * x5(t-1) + 0.0459 * x5(t-2) + -0.4995 * x1(t-2)*x1(t-2) + -0.0350 * x1(t-3)*x1(t-3) + e4(t)
x5(t) = -0.3512 * x4(t-1) + 0.3516 * x5(t-1) + -0.0006 * x2(t-1)*x5(t-1) + -0.0016 * x4(t-1)*x5(t-5) + 0.0006 * x4(t-5)*x5(t-1) + e5(t)
